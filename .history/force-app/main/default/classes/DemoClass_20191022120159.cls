/**
 * @File Name          : DemoClass.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 22/10/2019, 12:01:59 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    22/10/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class DemoClass {
    public DemoClass() {
        System.debug('TestClass Out');
    }
}
